<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>URL Shortener</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

</head>

<body>

<div class="container mt-4">
      <div class="row">
            <div class="col-md-12">
            @if (session('shortened_url'))
        <p>Shortened URL: <a href="{{ session('shortened_url') }}">{{ session('shortened_url') }}</a></p>
    @endif

    <form action="{{ route('url.shorten') }}" method="post">
        @csrf
        <label for="original_url">Enter URL to shorten:</label>
        <input type="url" name="original_url" id="original_url" required>
        <button type="submit">Shorten URL</button>
    </form>

    <hr>

    @php
          $urls = App\Models\Url::all();
    @endphp

      <table class="table table-bordered">
            <thead class="bg-dark">
                  <tr class="text-white text-center">
                        <th scope="col">Original Url</th>
                        <th scope="col">Shortened Url</th>
                  </tr>
            </thead>
            <tbody>
                 @foreach ($urls as $item)
                  <tr>
                        <td>{{ $item->original_url }}</td>
                        <td>{{ $item->shortened_url }}</td>
                  </tr>
                 @endforeach
            </tbody>
      </table>

            </div>
      </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

</body>
</html>
