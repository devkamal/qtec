@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">Expense</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">All Expense</li>
               </ol>
            </nav>
         </div>
      </div>
      <!--end breadcrumb-->

     <div class="d-flex justify-content-between">
            <h6 class="mb-0 text-uppercase">Expense Update</h6>
            <a href="{{ route('view_expense') }}" class="mb-0 text-uppercase btn btn-primary btn-sm">Expense List</a>
     </div>
    
      <hr>
      <div class="card">
         <div class="card-body">
            <div class="table-responsive">
               <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5">
                 
                  <form action="{{route('update_expense',$editData->id)}}" method="post">
                        @csrf

                        <div class="modal-body text-dark">

                        <div class="col-md-12">
                        <select name="category_id" id="category_id" class="form-control">
                              <option value="">Select Category</option>
                              @foreach ($categories as $item)
                                    <option value="{{ $item->id }}" {{ $item->id == $editData->category_id ? 'selected' : ''}}>{{ $item->category_name }}</option>
                              @endforeach
                        </select>
                        @error('category_id')
                              <span style="color:red">{{ $message }}</span>
                        @enderror
                        </div>


                        <div class="col-md-12">
                              <label for="name">Expense Name</label>
                              <input type="text" name="expense_name" value="{{$editData->expense_name}}" id="name" class="form-control">
                        </div>

                        </div>
                        <div class="modal-footer">
                              <button type="submit" class="btn btn-dark" id="updateCoupon">Update</button>
                        </div>
                  </form>
                
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection
