@extends('admin.master')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<div class="page-wrapper">
      <div class="page-content"> 
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                  <div class="breadcrumb-title pe-3">RoleWise Admin Profile</div>
                  <div class="ps-3">
                        <nav aria-label="breadcrumb">
                              <ol class="breadcrumb mb-0 p-0">
                                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">RoleWise Admin Profilep</li>
                              </ol>
                        </nav>
                  </div>
            </div>
            <!--end breadcrumb-->
            <div class="container">
                  <div class="main-body">
                        <div class="row">
                              <div class="col-lg-12">
                                    <form action="{{ route('rolewise.admin.store') }}" method="post">
                                          @csrf
                                          <div class="card">
                                                <div class="card-body">
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">User Name</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="name" class="form-control" placeholder="Enter your name">
                                                            </div>
                                                      </div>
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Full Name</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="username" class="form-control" placeholder="Enter your username">
                                                            </div>
                                                      </div>
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Email</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="email" class="form-control" placeholder="Enter your Email">
                                                            </div>
                                                      </div>
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Phone</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="phone" class="form-control" placeholder="Enter your phone">
                                                            </div>
                                                      </div>
                                                
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Address</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="address" class="form-control" placeholder="Enter your address">
                                                            </div>
                                                      </div>

                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Password</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="password" name="password" class="form-control" placeholder="Enter your password">
                                                            </div>
                                                      </div>

                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Assign Roles</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                 <select name="roles" id="roles" class="form-control">
                                                                  <option value="">User Roles</option>
                                                                  @foreach ($roles as $role)
                                                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                                  @endforeach
                                                                 </select>
                                                            </div>
                                                      </div>

                                                      <div class="row">
                                                            <div class="col-sm-3"></div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="submit" class="btn btn-primary px-4" value="Save Changes">
                                                            </div>
                                                      </div>
                                                </div>
                                          </div>
                                    </form>
                              </div>
                        </div>
                  </div>
            </div>
      </div>
</div>


@endsection