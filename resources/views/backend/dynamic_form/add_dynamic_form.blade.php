@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <div class="row">
         <div class="col-xl-12 mx-auto">
            <h6 class="mb-0 text-uppercase">Add New Dynamic Form</h6>
            <hr/>
            <div class="card">
               <div class="card-body">
                  <div class="p-4 border rounded">
                     <form action="{{ route('store_dynamic_form') }}" class="row g-3 needs-validation" method="post" novalidate>
                        @csrf
                    
                        <div class="col-md-4">
                           <label for="date" class="form-label"> Date <span class="text-danger">*</span></label>
                           <input type="date" class="form-control" name="date" id="date" required>
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                        <div class="col-md-4">
                           <label for="name" class="form-label">Name<span class="text-danger">*</span></label>
                           <input type="text" class="form-control" name="name" id="name" required>
                           <div class="valid-feedback">Looks good!</div>
                        </div>

                        <div class="col-md-4">
                           <label for="address" class="form-label">Address <span class="text-danger">*</span></label>
                           <input type="text" class="form-control" name="address" id="address" required>
                           <div class="valid-feedback">Looks good!</div>
                        </div>


                      <div class="row mt-4">
                      <div class="col-md-6">
                           <label for="category_id" class="form-label">Category Type <span class="text-danger">*</span></label>
                           <select class="form-select" name="category_id" id="category_id" required class="form-control" onchange="getCategory()">
                              <option value="">Select Category</option>
                              @foreach ($categories as $item)
                              <option value="{{ $item->id }}">{{ $item->category_name}}</option>
                              @endforeach
                           </select>
                           @error('category_id')
                           <span class="text-danger">{{ $message }}</span>
                           @enderror
                        </div>
                        <div class="col-md-6">
                           <label for="form_no" class="form-label">Form No <span class="text-danger">*</span></label>
                           <input type="text" class="form-control" name="form_no" id="form_no" value="{{ form_no() }}" required>
                        </div>
                      </div>

                        
                        <hr>
                        <!-------------Start calculation table------------------->     
                        <table class="table table-bordered" id="dynamic-table">
                           <thead class="bg-dark">
                              <tr class="text-white text-center">
                                 <th scope="col">Particular of Expenses</th>
                                 <th scope="col">Description</th>
                                 <th scope="col">ORG Name</th>
                                 <th scope="col">ORG Email</th>
                                 <th scope="col" colspan="1" class="text-center">Action</th>
                              </tr>
                           </thead>
                           <tbody id="expense-details-body">
                           </tbody>
                        </table>
                        <!-------------End calculation table------------------->                                  
                     </div>
              
                     <div class="col-12">
                        <button class="btn btn-primary" type="submit">Submit form</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>


<script>
   function getCategory() {
       var categoryId = document.getElementById('category_id').value;
   
       // Make an AJAX request to fetch data based on the selected category
       fetch('/get-expense-details/' + categoryId)
           .then(response => response.json())
           .then(data => {
               // Populate the table with the fetched data
               populateTable(data);
           })
           .catch(error => console.error('Error:', error));
   }
   
   function populateTable(data) {
       var tbody = document.getElementById('expense-details-body');
       tbody.innerHTML = ''; // Clear previous content
   
       // Iterate over the data and create table rows
       data.forEach(row => {
           var tr = document.createElement('tr');
   
           // Create and append table cells with input fields
           var td1 = document.createElement('td');
            var inputExpenseName = createInput('text', 'expense_name[]', 'form-control');
            inputExpenseName.value = row.expense_name; // Set the value from the data
            td1.appendChild(inputExpenseName);
            tr.appendChild(td1);
   
           var td2 = document.createElement('td');
           var inputDesc = createInput('text', 'description[]', 'form-control');
           td2.appendChild(inputDesc);
           tr.appendChild(td2);

           var td3 = document.createElement('td');
           var inputOrgName = createInput('text', 'orgname[]', 'form-control');
           td3.appendChild(inputOrgName);
           tr.appendChild(td3);

           var td4 = document.createElement('td');
           var inputOrgEmail = createInput('email', 'orgemail[]', 'form-control');
           td4.appendChild(inputOrgEmail);
           tr.appendChild(td4);
   
           var td7 = document.createElement('td');
           var minusButton = createButton('button', 'btn btn-danger', 'X', removeRow);
           td7.appendChild(minusButton);
           tr.appendChild(td7);
   
           tbody.appendChild(tr);
       });
   }
   
   function createInput(type, name, className) {
       var input = document.createElement('input');
       input.type = type;
       input.name = name;
       input.className = className;
       return input;
   }
   
   function createButton(type, className, text, clickFunction) {
       var button = document.createElement('button');
       button.type = type;
       button.className = className;
       button.textContent = text;
       button.addEventListener('click', clickFunction);
       return button;
   }
   
   function removeRow() {
   var row = this.parentNode.parentNode;
   row.parentNode.removeChild(row);
   }
   
   
</script>


<!-- // category ways form No -->
 <script>
    var formNoMap = {};

    document.getElementById('category_id').addEventListener('change', function () {
        var selectedOption = this.options[this.selectedIndex];
        var categoryName = selectedOption.text.trim().toUpperCase();
        var billNoInput = document.getElementById('form_no');

        // Check if the bill number is already generated for the selected category
        if (categoryName in formNoMap) {
            billNoInput.value = formNoMap[categoryName];
        } else {
            var dynamicBillNo = generateDynamicFormNo();
            var currentYear = new Date().getFullYear().toString().slice(-2);
            var startBillNo = 10001;

            var last7DigitsFromCategory = categoryName.slice(-9);
            formNoMap[categoryName] = last7DigitsFromCategory + '/' + currentYear + '-' + (startBillNo + dynamicBillNo);
            billNoInput.value = formNoMap[categoryName];
        }
    });

    function generateDynamicFormNo() {
        return Math.floor(Math.random() * 1000);
    }
</script>

<script>
   	(function () {
   	  'use strict'
   	  var forms = document.querySelectorAll('.needs-validation')
   	  Array.prototype.slice.call(forms)
   		.forEach(function (form) {
   		  form.addEventListener('submit', function (event) {
   			if (!form.checkValidity()) {
   			  event.preventDefault()
   			  event.stopPropagation()
   			}
   			form.classList.add('was-validated')
   		  }, false)
   		})
   	})()
</script>



@endsection


