@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <div class="row">
         <div class="col-xl-12 mx-auto">
           <div class="d-flex justify-content-between">
                <h6 class="mb-0 text-uppercase">Details Dynamic Form</h6>
                <h6><a href="{{ route('view_dynamic_form') }}">Back</a></h6>
           </div>
            <hr/>
            <div class="card">
               <div class="card-body">
                  <div class="p-4 border rounded">
                     <form  class="row g-3 needs-validation" method="post" novalidate>
                        @csrf
                        <div class="col-md-4">
                           <label for="date" class="form-label"> Date </label>
                           <input type="date" class="form-control" value="{{ $details->date }}" readonly>
                        </div>
                        <div class="col-md-4">
                           <label for="name" class="form-label">Name</label>
                           <input type="text" class="form-control" value="{{ $details->name }}" readonly>
                        </div>

                        <div class="col-md-4">
                           <label for="address" class="form-label">Address </label>
                           <input type="text" class="form-control"  value="{{ $details->address }}" readonly>
                        </div>


                     <div class="row">
                     <div class="col-md-6">
                            <label for="category_id" class="form-label">Category Type </label>
                            <select class="form-select" readonly required class="form-control" onchange="getCategory()">
                                @foreach ($categories as $item)
                                    <option value="{{ $item->id }}" {{ $item->id == $details->category_id ? 'selected' : ''}}>{{ $item->category_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-6">
                            <label for="bill_no" class="form-label">Form No </label>
                            <input type="text" class="form-control" readonly value="{{ $details->form_no }}" readonly>
                        </div>
                     </div>


                    
                        <hr>

                          <!-------------Start calculation table------------------->     
                            <table class="table table-bordered" id="dynamic-table">
                                <thead class="bg-dark">
                                    <tr class="text-white text-center">
                                       <th scope="col">Particular of Expenses</th>
                                       <th scope="col">Description</th>
                                       <th scope="col">ORG Name</th>
                                       <th scope="col">ORG Email</th>
                                       </tr>
                                </thead>
                                @foreach ($details->formDetails as $expense)
                                    <tr class="text-center">
                                          <td>{{ $expense->expense_name }}</td>
                                          <td>{{ $expense->description }}</td>
                                          <td>{{ $expense->orgname }}</td>
                                          <td>{{ $expense->orgemail }}</td>
                                    </tr>
                              @endforeach
                                                              
                            </div>
                         
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection