@extends('admin.master')
@section('content')

<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">Export Bill</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">All Export Bill</li>
               </ol>
            </nav>
         </div>
      
      </div>
      <!--end breadcrumb-->

     <div class="d-flex justify-content-between">
            <h6 class="mb-0 text-uppercase">Export Bill List</h6>
     </div>
    
      <hr>
      <div class="card">
         <div class="card-body">
            <div class="table-responsive">
               <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5">
                 
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="example" class="table table-striped table-bordered dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">
                        <thead>
                              <tr role="row">
                                 <th>Sl</th>
                                 <th>Date</th>
                                 <th>Name</th>
                                 <th> Address</th>
                                 <th> Category Name</th>
                                 <th> Form No</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach ($allData as $key => $item)
                                    <tr role="row" class="odd">
                                       <td>{{ $key+1 }}</td>
                                       <td>{{ $item->date }}</td>
                                       <td>{{ $item->name }}</td>
                                       <td>{{ $item->address }}</td>
                                       <td>{{ $item['category']['category_name'] }}</td>
                                       <td>{{ $item->form_no }}</td>
                                       <td>
                                          <a href="{{ route('details_dynamic_form',$item->id) }}" title="view" class="btn btn-primary btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                          <a href="{{ route('delete_dynamic_form',$item->id) }}" id="delete" title="delete" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                       </td>
                                    </tr>

                              @endforeach
                           </tbody>
                    
                        </table>
                        {{ $allData->links('pagination_link') }}
                     </div>
                  </div>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
 
 
@endsection
