<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!--favicon-->
      <link rel="icon" href="{{ asset('backend/assets/images/favicon-32x32.png') }}" type="image/png" />
      <!--plugins-->
      <link href="{{ asset('backend/assets/plugins/simplebar/css/simplebar.css') }}" rel="stylesheet" />
      <link href="{{ asset('backend/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet" />
      <link href="{{ asset('backend/assets/plugins/metismenu/css/metisMenu.min.css') }}" rel="stylesheet" />
      <!-- loader-->
      <link href="{{ asset('backend/assets/css/pace.min.css') }}" rel="stylesheet" />
      <script src="{{ asset('backend/assets/js/pace.min.js') }}"></script>
      <!-- Bootstrap CSS -->
      <link href="{{ asset('backend/assets/css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="{{ asset('backend/assets/css/app.css') }}" rel="stylesheet">
      <link href="{{ asset('backend/assets/css/icons.css') }}" rel="stylesheet">
      <title>Admin Login</title>
   </head>
   <body class="bg-login">
      <!--wrapper-->
      <div class="wrapper">
         <div class="section-authentication-signin d-flex align-items-center justify-content-center my-5 my-lg-4">
            <div class="container-fluid">
               <div class="row row-cols-1 row-cols-lg-2 row-cols-xl-3">
                  <div class="col mx-auto">
                     <div class="card mt-5 mt-lg-0">
                        <div class="card-body">
                           <div class="border p-4 rounded">
                              <div class="text-center">
                                 <h3 class="">Admin Sign in</h3>
                              </div>
                           </div>
                           <div class="form-body">
                              <form method="POST" action="{{ route('login') }}" class="row g-3">
                                 @csrf
                                 <div class="col-12">
                                    <label for="email" class="form-label">Email Address</label>
                                    <input class="form-control" id="email" type="email" name="email"
                                       required autofocus autocomplete="email" placeholder="Email Address">
                                 </div>
                                 <div class="col-12">
                                    <label for="password" class="form-label">Enter Password</label>
                                    <div class="input-group" id="show_hide_password">
                                       <input id="password" type="password" name="password"
                                          required autocomplete="current-password" class="form-control border-end-0" 
                                          placeholder="Enter Password"> 
                                       <a href="javascript:;" class="input-group-text bg-transparent">
                                       <i class='bx bx-hide'></i>
                                       </a>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-check form-switch">
                                       <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked>
                                       <label class="form-check-label" for="flexSwitchCheckChecked">Remember Me</label>
                                    </div>
                                 </div>
                                 <div class="col-md-6 text-end">	<a href="authentication-forgot-password.html">Forgot Password ?</a>
                                 </div>
                                 <div class="col-12">
                                    <div class="d-grid">
                                       <button type="submit" class="btn btn-primary"><i class="bx bxs-lock-open"></i>Sign in</button>
                                    </div>
                                 </div>
                              </form>
                              <!---------click for login --------->
                              <br>
                              <div class="panel-footer m-t-10">
                                 <table class="table table-bordered">
                                    <thead>
                                       <tr>
                                          <th>Email</th>
                                          <th>Password</th>
                                          <th>Role</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr>
                                          <td>admin@gmail.com</td>
                                          <td>12345678</td>
                                          <td>Admin</td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>

                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--end row-->
         </div>
      </div>
      </div>
      <!--end wrapper-->
      <!-- Bootstrap JS -->
      <script src="{{ asset('backend/assets/js/bootstrap.bundle.min.js') }}"></script>
      <!--plugins-->
      <script src="{{ asset('backend/assets/js/jquery.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/simplebar/js/simplebar.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/metismenu/js/metisMenu.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js') }}"></script>
      <!--Password show & hide js -->
      <script>
         $(document).ready(function () {
               $("#show_hide_password a").on('click', function (event) {
                     event.preventDefault();
                     if ($('#show_hide_password input').attr("type") == "text") {
                           $('#show_hide_password input').attr('type', 'password');
                           $('#show_hide_password i').addClass("bx-hide");
                           $('#show_hide_password i').removeClass("bx-show");
                     } else if ($('#show_hide_password input').attr("type") == "password") {
                           $('#show_hide_password input').attr('type', 'text');
                           $('#show_hide_password i').removeClass("bx-hide");
                           $('#show_hide_password i').addClass("bx-show");
                     }
               });
         });
      </script>
      <!--app JS-->
      <script src="{{ asset('backend/assets/js/app.js') }}"></script>

      <!--------========Click email & password for admin login============----------->
      <script type="text/javascript">
      $(document).ready(function() {
         var info = $('table tbody tr');
         info.click(function() {
            var email    = $(this).children().first().text();
            var password = $(this).children().first().next().text();

            $("input[type=email]").val(email);
            $("input[type=password]").val(password);
         });
      });
      </script>

   </body>
</html>