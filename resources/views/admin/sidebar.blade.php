<div class="sidebar-wrapper" data-simplebar="true">
   <div class="sidebar-header">
      <div>
         <img src="{{asset('backend/logo.png')}}" class="logo-icon" alt="logo icon">
      </div>
   
      <div class="toggle-icon ms-auto"><i class='bx bx-arrow-to-left'></i>
      </div>
   </div>

   <!--navigation-->
   <ul class="metismenu" id="menu">
      <li>
         <a href="{{ route('admin.dashboard') }}">
            <div class="parent-icon"><i class='bx bx-home-circle'></i>
            </div>
            <div class="menu-title">Dashboard</div>
         </a>
      </li>

      <hr>

      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Manage Category</div>
         </a>
         <ul>
            <li> <a href="{{ route('view_category') }}"><i class="bx bx-right-arrow-alt"></i>Add Category</a></li>
         </ul>
      </li>

      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Manage Expense</div>
         </a>
         <ul>
            <li> <a href="{{ route('view_expense') }}"><i class="bx bx-right-arrow-alt"></i>Add Expense</a></li>
         </ul>
      </li>

      <hr>

      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Manage Dynamic Form</div>
         </a>
         <ul>
            <li> <a href="{{ route('add_dynamic_form') }}"><i class="bx bx-right-arrow-alt"></i>Add New Form</a></li>

            <li> <a href="{{ route('view_dynamic_form') }}"><i class="bx bx-right-arrow-alt"></i>Form List</a></li>
         </ul>
      </li>


      <br><br><br><br><br><br>
   </ul>
   <!--end navigation-->
</div>