1. dynamic_forms Table
Purpose:
The migration file named create_dynamic_forms_table.php is responsible for creating the dynamic_forms table. This table is designed to store information related to dynamic forms, which may be used to collect various details.
id: Primary key for the table.
date: Date associated with the dynamic form.
name: Name associated with the dynamic form.
address: Address associated with the dynamic form.
category_id: Foreign key referencing the 'id' column in the 'categories' table.
form_no: A string representing the form number.
timestamps: Created at and Updated at timestamps.

2. dynamic_form_details Table
Purpose:
The migration file named create_dynamic_form_details_table.php is responsible for creating the dynamic_form_details table. This table is designed to store detailed information related to expenses associated with the dynamic forms.
id: Primary key for the table.
dynamic_form_id: Foreign key referencing the 'id' column in the 'dynamic_forms' table.
expense_name: Name associated with the expense.
description: Detailed description of the expense.
orgname: Name of the organization related to the expense.
orgemail: Email associated with the organization.
timestamps: Created at and Updated at timestamps.

3. urls Table
Purpose:
The migration file named create_urls_table.php is responsible for creating the urls table. This table is designed to store information related to URLs, with a focus on creating a mapping between original URLs and their corresponding shortened versions.
id: Primary key for the table.
original_url: The original URL that is being shortened.
shortened_url: The unique shortened version of the original URL.
timestamps: Created at and Updated at timestamps.
Explanation:
id: This is an auto-incrementing primary key that uniquely identifies each record in the 'urls' table.

original_url: This column is used to store the original, long URL that needs to be shortened.

shortened_url: This column is used to store the unique, shortened version of the original URL. It is marked as unique to ensure that each shortened URL is distinct.

timestamps: Laravel automatically maintains 'created_at' and 'updated_at' timestamps for each record in the table, providing information about when the record was created and last updated.

Usage:
This 'urls' table can be utilized to create a URL shortening service. Users can input a long URL, and the system will generate a unique shortened URL. The uniqueness constraint on 'shortened_url' ensures that each original URL is associated with only one shortened URL.
