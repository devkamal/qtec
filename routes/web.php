<?php

use App\Models\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UrlController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProfileController;
use App\Http\Middleware\RedirectIfAuthenticated;
use App\Http\Controllers\Frontend\UserController;
use App\Http\Controllers\Frontend\LoginController;
use App\Http\Controllers\Backend\ExpenseController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\DynamicFormController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


require __DIR__.'/auth.php';

//Admin middleware
Route::middleware(['auth', 'role:admin'])->group(function () {
    //Admin Dashboard
    Route::controller(AdminController::class)->group(function () {
        Route::get('/admin/dashboard', 'AdminDashboard')->name('admin.dashboard');
        Route::get('/logout', 'AdminDestroy')->name('admin.logout');
        Route::get('/admin/profile','AdminProfile')->name('admin_profile');
        Route::post('/admin/store','AdminStore')->name('admin_store');
        Route::get('/admin/password','AdminPassword')->name('password_change');
        Route::post('/update/password','updatePassword')->name('update_password');
    });


    //Category routing
    Route::controller(CategoryController::class)->prefix('category')->group(function(){
        Route::get('/view','view')->name('view_category');
        Route::post('/store','store')->name('store_category');
        Route::post('/update/{id}','update')->name('update_category');
        Route::get('/delete/{id}','delete')->name('delete_category');
    });

    //Category routing
    Route::controller(ExpenseController::class)->prefix('expense')->group(function(){
        Route::get('/view','view')->name('view_expense');
        Route::post('/store','store')->name('store_expense');
        Route::get('/update/{id}','edit')->name('edit_expense');
        Route::post('/update/{id}','update')->name('update_expense');
        Route::get('/delete/{id}','delete')->name('delete_expense');
    });

    Route::get('/get-expense-details/{categoryId}', [ExpenseController::class, 'getExpenseDetails']);


    //Exportbill routing
    Route::controller(DynamicFormController::class)->prefix('exportbill')->group(function () {
        Route::get('/view', 'view')->name('view_dynamic_form');
        Route::get('/add', 'add')->name('add_dynamic_form');
        Route::post('/store', 'store')->name('store_dynamic_form');
        Route::get('/details/{id}', 'detailsView')->name('details_dynamic_form');
        Route::get('/delete/{id}', 'delete')->name('delete_dynamic_form');
    });
    /*************End Export*******************/


});  //End Admin middleware

Route::get('admin/login', [AdminController::class, 'AdminLogin'])->name('admin.login')->middleware(RedirectIfAuthenticated::class);

// url shortener system
Route::get('/url', [UrlController::class, 'index'])->name('url.index');
Route::post('/shorten', [UrlController::class, 'shorten'])->name('url.shorten');
Route::get('/{shortenedUrl}', [UrlController::class, 'redirectToOriginalUrl']);


