<?php

namespace App\Http\Controllers;

use App\Models\Url;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class UrlController extends Controller
{
    public function index()
    {
        return view('url.index');
    }

    public function shorten(Request $request)
    {
        $request->validate([
            'original_url' => 'required|url',
        ]);

        $url = Url::create([
            'original_url' => $request->input('original_url'),
            'shortened_url' => $this->generateShortUrl(),
        ]);

        return redirect()->route('url.index')->with('shortened_url', $url->shortened_url);
    }

    private function generateShortUrl()
    {
        return Str::random(6);
    }

    public function redirectToOriginalUrl($shortenedUrl)
    {
        $url = Url::where('shortened_url', $shortenedUrl)->first();

        if (!$url) {
            abort(404);
        }

        return redirect($url->original_url);
    }
}
