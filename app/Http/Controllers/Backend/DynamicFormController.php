<?php

namespace App\Http\Controllers\Backend;

use App\Models\Category;
use App\Models\DynamicForm;
use Illuminate\Http\Request;
use App\Models\DynamicFormDetail;
use App\Http\Controllers\Controller;

class DynamicFormController extends Controller
{
    public function view(){
        $allData = DynamicForm::orderBy('id','desc')->paginate(10);
        return view('backend.dynamic_form.view_dynamic_form',compact('allData'));
       }
    
       public function add(){
        $categories = Category::all();
        return view('backend.dynamic_form.add_dynamic_form',compact('categories'));
       }
       

       public function store(Request $request)
       {
            //dd($request->all());
          
            $validateData = $request->validate([
                'category_id' => 'required',
            ]);

            $dynamicForm = DynamicForm::create([
               'date' => $request->date,
               'name' => $request->name,
               'address' => $request->address,
               'category_id' => $request->category_id,
               'form_no' => $request->form_no,
           ]);
   
           // Create DynamicFormDetail
           $formDetails = [];
           foreach ($request->expense_name as $key => $expense_name) {
               $formDetails[] = new DynamicFormDetail([
                   'expense_name' => $expense_name,
                   'description' => $request->description[$key],
                   'orgname' => $request->orgname[$key],
                   'orgemail' => $request->orgemail[$key],
               ]);
           }
   
        $dynamicForm->formDetails()->saveMany($formDetails);
   
          
         $notification = array(
            'message' => 'dynamicForm Inserted Successfully',
               'alert-type' => 'success'
         );
   
         return redirect()->route('view_dynamic_form')->with($notification); 
       }


       public function detailsView($id){
        $details = DynamicForm::with('formDetails')->findOrFail($id);
        $categories = Category::all();
        return view('backend.dynamic_form.details_dynamic_form',compact('details','categories'));
       }
       
    
    public function delete($id){
       $product = DynamicForm::findOrFail($id);
       DynamicForm::findOrFail($id)->delete();
    
       $details = DynamicFormDetail::where('dynamic_form_id',$id)->get();
       foreach($details as $item){
           DynamicFormDetail::where('dynamic_form_id',$id)->delete();
       }
    
       $notification = array(
           'message' => 'Dynamic Form Deleted Successfully',
           'alert-type' => 'success'
       );
    
       return redirect()->back()->with($notification);
    }
    
}
