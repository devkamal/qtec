<?php

namespace App\Http\Controllers\Backend;

use App\Models\Expense;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExpenseController extends Controller
{
    public function view(){
        $allData = Expense::orderBy('id','desc')->paginate(5);
        $categories = Category::all();
        return view('backend.expense.view_expense',compact('allData','categories'));
    }

    public function store(Request $request){
        $validateData = $request->validate([
            'category_id'  => 'required',
            'expense_name'  => 'required',
        ]);

        $inputData = new Expense();
        $inputData->category_id    = $request->category_id;
        $inputData->expense_name    = $request->expense_name;
        $inputData->save();

        $notification = array(
            'message' => 'Expense added successfully.',
            'alert-type' => 'success',
        );
        return redirect()->back()->with($notification);
    }

    public function edit($id){
        $categories = Category::all();
        $editData = Expense::find($id);
        return view('backend.expense.edit_expense',compact('editData','categories'));
    }

    public function update(Request $request, $id){
        $updateData = Expense::findOrFail($id);
        $updateData->category_id  = $request->category_id;
        $updateData->expense_name = $request->expense_name;
        $updateData->update();

        $notification = array(
            'message' => 'Expense update successfully.',
            'alert-type' => 'success',
        );
        return redirect()->route('view_expense')->with($notification);
    }

    public function delete($id){
        Expense::findOrFail($id)->delete();
        $notification = array(
            'message' => 'Expense remove successfully.',
            'alert-type' => 'success',
        );
        return redirect()->back()->with($notification);
    }

    //Get Expense Name
    public function GetExpenseName($id){
        $expense = Expense::find($id);

        if ($expense) {
            return response()->json(['expense_name' => $expense->expense_name]);
        } else {
            return response()->json(['error' => 'ExpenseName not found'], 404);
        }
    }

    public function getExpenseDetails($categoryId)
    {
        // Fetch expense details based on the selected category
        $expenseDetails = Expense::where('category_id', $categoryId)->get();

        return response()->json($expenseDetails);
    }

}
