<?php

namespace App\Http\Controllers\Backend;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function view(){
        $allData = Category::orderBy('id','desc')->paginate(5);
        return view('backend.category.view_category',compact('allData'));
    }

    public function store(Request $request){
        $validateData = $request->validate([
            'category_name'  => 'required',
        ]);

        $inputData = new Category();
        $inputData->category_name    = $request->category_name;
        $inputData->save();

        $notification = array(
            'message' => 'Category added successfully.',
            'alert-type' => 'success',
        );
        return redirect()->back()->with($notification);
    }

    public function update(Request $request, $id){
        $updateData = Category::findOrFail($id);
        $updateData->category_name    = $request->category_name;
        $updateData->update();

        $notification = array(
            'message' => 'Category update successfully.',
            'alert-type' => 'success',
        );
        return redirect()->back()->with($notification);
    }

    public function delete($id){
        Category::findOrFail($id)->delete();
        $notification = array(
            'message' => 'Category remove successfully.',
            'alert-type' => 'success',
        );
        return redirect()->back()->with($notification);
    }

}
