<?php

use App\Models\DynamicForm;

  function form_no(){
    $bill = DynamicForm::orderBy('id','desc')->first();

    if($bill){
        $form_no = (int)$bill->invoice_no + 1;
    } else {
        $form_no = 1;
    }

    // Format the bill number with leading zeros
    $formatted_bill_no = str_pad($form_no, 5, '0', STR_PAD_LEFT);

    return $formatted_bill_no;
}
  